// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

  /** Constants for ports that are on the roboRIO. */
  public static final class RoboRIO {
    /** Constants for the Control Area Network bus. */
    public static final class CAN {
      /** The port of the back left drive motor. */
      public static final int kPortMotorDriveBackLeft = 1;
      /** The port of the front right drive motor. */
      public static final int kPortMotorDriveFrontRight = 2;
      /** The port of the back right drive motor. */
      public static final int kPortMotorDriveBackRight = 3;
      /** The port of the left hanger motor. */
      public static final int kPortMotorHangerLeft = 5;
      /** The port of the right hanger motor. */
      public static final int kPortMotorHangerRight = 6;
      /** The port of the front left drive motor. */
      public static final int kPortMotorDriveFrontLeft = 7;
      /** The ports of the intake double solenoid. */
      public static final int[] kPortDoubleSolenoidIntake = {0, 1};
      /** The port of the intake motor. */
      public static final int kPortMotorIntake = 4;
      /** The ports of the the launcher double solenoid. */
      public static final int[] kPortDoubleSolenoidLauncher = {2, 3};
    }

    /** Constants for the Digital Input/Output ports. */
    public static final class DIO {
      /** The port of the left hanger servo. */
      public static final int kPortServoLeftHanger = 1;
      /** The port of the right hanger servo. */
      public static final int kPortServoRightHanger = 2;
      /** The port of the lower intake limit switch. */
      public static final int kPortLimitSwitchLowerIntake = 6;
      /** The port of the upper intake limit switch. */
      public static final int kPortLimitSwitchUpperIntake = 7;
      /** The port of the right hanger limit switch. */
      public static final int kPortLimitSwitchRightHanger = 8;
      /** The port of the left hanger limit switch. */
      public static final int kPortLimitSwitchLeftHanger = 9;
    }
  }

  /** Constants for ports that are on the driver station. */
  public static final class DriverStation {
    /** The port of the driver controller. */
    public static final int kPortControllerDrive = 0;
    /** The port of the manipulator controller. */
    public static final int kPortControllerManip = 1;
  }

  /**
   * Constants for the drivetrain subsystem.
   *
   * <p>Since these measurements are prone to unit errors, they are listed before each variable is
   * defined.
   */
  public static final class DrivetrainConstants {
    // Speed Constants
    /**
     * Lowest bound to set drive motors.
     *
     * <p>Unitless.
     */
    public static final double kSpeedSlow = 0.3;
    /**
     * Normal bound to set drive motors.
     *
     * <p>Unitless.
     */
    public static final double kSpeedNormal = 0.5;
    /**
     * Highest bound to set drive motors.
     *
     * <p>Unitless.
     */
    public static final double kSpeedFast = 0.95;
    /**
     * The time required for the robot to back out of the initial tarmac area with the normal speed
     * modifier during the autonomous mode.
     *
     * <p>Used for dead reckoning purposes.
     *
     * <p>Seconds.
     */
    public static final double kTimeLeaveTarmac = 2.5;
  }

  /** Constants for the hanger subsystem. */
  public static final class HangerConstants {
    /** Counts per revolution of the hanger encoders. */
    public static final int kEncoderCyclesPerRev = 8192;
    /** Bound to set the hanger motors when extending. */
    public static final double kSpeedExtend = 0.25;
    /** Bound to set the hanger motors when retracting. */
    public static final double kSpeedRetract = 0.5;
    /** Encoder reading when extending the hanger. */
    public static final double kEncodersRaised = 11.0;
    /** Servo position for the left hanger mechanical stop. */
    public static final double kServoLeftLocked = 0.2;
    /** Servo position for the right hanger mechanical stop. */
    public static final double kServoRightLocked = 0.25;
    /** Servo position for when the left hanger mechanical is not stopped. */
    public static final double kServoLeftUnlocked = 0.4;
    /** Servo position for when the right hanger mechanical is not stopped. */
    public static final double kServoRightUnlocked = 0.0;
    /** Diamater of the hanger hex shaft. */
    public static final double kHexShaftDiam = 0.5;
  }

  /** Constants for the intake subystem. */
  public static final class IntakeConstants {
    /** Bound to set the intake motor when raising. */
    public static final double kSpeedRaise = 0.75;
    /**
     * Bound to set the intake motor when lowering.
     *
     * <p>The speed is represented as negative so that the motor is turned clockwise.
     */
    public static final double kSpeedLower = -0.5;
  }

  /**
   * Constants for the launcher subsystem.
   *
   * <p>Since these measurements are prone to unit errors, they are listed before each variable is
   * defined.
   */
  public static final class LauncherConstants {
    /**
     * The time required for the launcher solenoid to extend or retract.
     *
     * <p>Seconds.
     */
    public static final double kTimeToggle = 1.0;
  }
}
