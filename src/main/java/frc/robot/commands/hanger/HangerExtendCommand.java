// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.commands.hanger;

import edu.wpi.first.wpilibj2.command.CommandBase;

import frc.robot.Constants.HangerConstants;
import frc.robot.subsystems.HangerSubsystem;

/** Extends the hanger until the motors' encoder values have reached a certain threshold. */
public class HangerExtendCommand extends CommandBase {

  // Subsystems
  private final HangerSubsystem m_hangerSubsystem;

  // Power applied to the hanger motors.
  private final double m_power;

  /**
   * Constructs a command with the hanger subsystem and proportion to use when setting hanger
   * motors.
   */
  public HangerExtendCommand(HangerSubsystem hangerSubsystem, double power) {
    m_hangerSubsystem = hangerSubsystem;
    m_power = power;

    addRequirements(m_hangerSubsystem);
  }

  /** Runs periodically while the command is scheduled. */
  @Override
  public void execute() {
    // Extend the hanger motors based on the given power.
    m_hangerSubsystem.extendHanger(m_power);
  }

  /** Runs when the command ends. */
  @Override
  public void end(boolean interrupted) {
    // Disable the hanger motors.
    m_hangerSubsystem.disableHanger();
  }

  /**
   * @return Whether the command has finished. Once a command finishes, the scheduler will call its
   *     end() method and un-schedule it.
   */
  @Override
  public boolean isFinished() {
    // End the command if the left and right encoders have reached an experimentally determined
    // reading.
    return (m_hangerSubsystem.getEncoderLeft() >= HangerConstants.kEncodersRaised
        && m_hangerSubsystem.getEncoderRight() >= -HangerConstants.kEncodersRaised);
  }
}
