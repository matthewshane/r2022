// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandGroupBase;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;

import frc.robot.Constants.DriverStation;
import frc.robot.Constants.DrivetrainConstants;
import frc.robot.Constants.HangerConstants;
import frc.robot.commands.AutoInitCommand;
import frc.robot.commands.DisabledInitCommand;
import frc.robot.commands.TeleopInitCommand;
import frc.robot.commands.drive.DriveLinearCommand;
import frc.robot.commands.hanger.HangerExtendCommand;
import frc.robot.commands.hanger.HangerRetractCommand;
import frc.robot.commands.launcher.LaunchCommand;
import frc.robot.input.F310Controller;
import frc.robot.input.F310Controller.Hand;
import frc.robot.subsystems.DrivetrainSubsystem;
import frc.robot.subsystems.DrivetrainSubsystem.DriveSpeed;
import frc.robot.subsystems.HangerSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.IntakeSubsystem.IntakeSpeed;
import frc.robot.subsystems.LauncherSubsystem;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // Subsystems
  private final DrivetrainSubsystem m_drivetrainSubsystem = new DrivetrainSubsystem();
  private final HangerSubsystem m_hangerSubsystem = new HangerSubsystem();
  private final IntakeSubsystem m_intakeSubsystem = new IntakeSubsystem();
  private final LauncherSubsystem m_launcherSubsystem = new LauncherSubsystem();

  // Commands
  private final AutoInitCommand m_autoInitCommand =
      new AutoInitCommand(m_drivetrainSubsystem, m_intakeSubsystem, m_launcherSubsystem);
  private final DisabledInitCommand m_disabledInitCommand =
      new DisabledInitCommand(m_drivetrainSubsystem, m_hangerSubsystem);
  private final TeleopInitCommand m_teleopInitCommand =
      new TeleopInitCommand(m_drivetrainSubsystem);

  // Controller Inputs
  private final F310Controller m_controllerDrive =
      new F310Controller(DriverStation.kPortControllerDrive);
  private final F310Controller m_controllerManip =
      new F310Controller(DriverStation.kPortControllerManip);

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    // Default Commands

    /** Sets drivetrain to arcade mode, with speed and rotation determined by driver's input. */
    m_drivetrainSubsystem.setDefaultCommand(
        new RunCommand(
                () ->
                    m_drivetrainSubsystem.arcadeDrive(
                        m_controllerDrive.getIntensityY(Hand.kLeft),
                        m_controllerDrive.getIntensityX(Hand.kRight)),
                m_drivetrainSubsystem)
            .withName("Arcade Drive"));

    // Configure buttons to command bindings.
    configureButtonBindings();
  }

  /** Configures {@link F310Controller} button mappings. */
  private void configureButtonBindings() {

    // Drivetrain speed modifiers.
    Command resetDriveSpeedCommand =
        new InstantCommand(
            () -> m_drivetrainSubsystem.setDriveSpeed(DriveSpeed.kNormal), m_drivetrainSubsystem);
    m_controllerDrive
        .axisLt
        .whileHeld(
            () -> m_drivetrainSubsystem.setDriveSpeed(DriveSpeed.kSlow), m_drivetrainSubsystem)
        .whenReleased(resetDriveSpeedCommand);
    m_controllerDrive
        .axisLt
        .whileHeld(
            () -> m_drivetrainSubsystem.setDriveSpeed(DriveSpeed.kFast), m_drivetrainSubsystem)
        .whenReleased(resetDriveSpeedCommand);

    // Intake extension and lifting.
    Command resetIntakeSpeedCommand =
        new InstantCommand(
            () -> m_intakeSubsystem.setIntakeSpeed(IntakeSpeed.kOff), m_intakeSubsystem);
    m_controllerManip
        .buttonB
        .whileHeld(() -> m_intakeSubsystem.setIntakeSpeed(IntakeSpeed.kRaise), m_intakeSubsystem)
        .whenReleased(resetIntakeSpeedCommand);
    m_controllerManip
        .buttonX
        .whileHeld(() -> m_intakeSubsystem.setIntakeSpeed(IntakeSpeed.kLower), m_intakeSubsystem)
        .whenReleased(resetIntakeSpeedCommand);
    m_controllerManip.buttonY.whenPressed(
        () -> m_intakeSubsystem.toggleGrabber(), m_intakeSubsystem);

    // Launching solenoid toggle.
    m_controllerManip.buttonA.whenPressed(new LaunchCommand(m_launcherSubsystem));

    // Hanging controls.
    m_controllerDrive.buttonA.whenPressed(
        new HangerExtendCommand(m_hangerSubsystem, HangerConstants.kSpeedExtend));
    m_controllerDrive.buttonB.whenHeld(
        new HangerRetractCommand(m_hangerSubsystem, HangerConstants.kSpeedRetract));
    m_controllerDrive.buttonX.whenPressed(
        new InstantCommand(
            () -> {
              m_hangerSubsystem.setLeftServo(HangerConstants.kServoLeftLocked);
              m_hangerSubsystem.setRightServo(HangerConstants.kServoRightLocked);
            },
            m_hangerSubsystem));
    m_controllerDrive.buttonY.whenPressed(
        new InstantCommand(
            () -> {
              m_hangerSubsystem.setLeftServo(HangerConstants.kServoLeftUnlocked);
              m_hangerSubsystem.setRightServo(HangerConstants.kServoRightUnlocked);
            },
            m_hangerSubsystem));
  }

  /**
   * Passes the autonomous command to the main {@link Robot} class.
   *
   * @return The command to run in autonomous.
   */
  public Command getAutoInitCommand() {
    // Clear stashed autonomous commands.
    CommandGroupBase.clearGroupedCommand(m_autoInitCommand);

    // Initialize values prior to executing the intended command.
    return m_autoInitCommand.andThen(
        // Dead reckoning autonomous command.
        new SequentialCommandGroup(
            // Reverse to a position such that the robot leaves the tarmac.
            new DriveLinearCommand(
                m_drivetrainSubsystem,
                -DriveSpeed.kNormal.value,
                DrivetrainConstants.kTimeLeaveTarmac),
            // Launch the cargo.
            new LaunchCommand(m_launcherSubsystem)));
  }

  /**
   * Passes the disabled init command to the main {@link Robot} class.
   *
   * @return The command to run when the robot is disabled.
   */
  public Command getDisabledInitCommand() {
    return m_disabledInitCommand;
  }

  /**
   * Passes the teleop init command to the main {@link Robot} class.
   *
   * @return The command to run in teleoperated mode.
   */
  public Command getTeleopInitCommand() {
    return m_teleopInitCommand;
  }
}
